function showCategories() {
    const container = document.querySelector('.categories');

    for (let i = 0; i < data.length; i++) {
        const elem = document.createElement('div');
        elem.textContent = data[i].name;
        const count = i.toString();
        elem.setAttribute('data-category', count);
        elem.addEventListener('click', showProducts);
        container.appendChild(elem);
    }
}

function showProducts(event) {
    const result = document.querySelector('.result');
    const categoryIndex = event.target.getAttribute('data-category');
    const products = data[categoryIndex].products;
    const container = document.querySelector('.products');
    container.innerHTML = '';
    result.innerHTML = '';

    for (let i = 0; i < products.length; i++) {
        const elem = document.createElement('div');
        elem.textContent = products[i].name;
        const count = i.toString()
        elem.setAttribute('data-product', count);
        elem.setAttribute('data-category', categoryIndex);
        elem.addEventListener('click', showDetails);
        container.appendChild(elem);
    }
}

function showDetails(event) {
    const categoryIndex = event.target.getAttribute('data-category');
    const productIndex = event.target.getAttribute('data-product');

    const products = document.querySelector('.products');
    const details = document.querySelector('.details');
    const result = document.querySelector('.result');

    const resultInfo = document.querySelector('.details');
    resultInfo.innerHTML = '';

    const productName = document.createElement('div');
    productName.textContent = data[categoryIndex].products[productIndex].name;

    const productPrice = document.createElement('div');
    productPrice.textContent = data[categoryIndex].products[productIndex].price;

    const productDescription = document.createElement('div');
    productDescription.textContent = data[categoryIndex].products[productIndex].description;

    const submitButton = document.createElement('button');
    submitButton.textContent = 'Bye';

    submitButton.addEventListener('click', showResult);

    function showResult() {
        products.innerHTML = ''
        details.innerHTML = ''
        result.innerHTML = 'Congratulations product purchased!!!'
    }

    resultInfo.appendChild(productName);
    resultInfo.appendChild(productPrice);
    resultInfo.appendChild(productDescription);
    resultInfo.appendChild(submitButton);

}

showCategories();


